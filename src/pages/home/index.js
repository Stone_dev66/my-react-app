import React from 'react'
import { connect } from 'react-redux'
import './index.scss'

import { addNum, decreaseNum } from '../../store/actions/app'

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    add() {

    }

    decrease() {

    }
    componentWillMount() {

    }

    render() {
        return (<div className="home-container">
            <div className="calc-group">
                <button className="op-left" onClick={this.props.decrease.bind(this)}>-</button>
                <div className="count">{this.props.num}</div>
                <button className="op-right" onClick={this.props.add.bind(this)}>+</button>
            </div>

        </div>)
    }

}

const mapStateToProps = (state) => ({
    num: state.app.num

})

const mapDispatchToProps = (dispatch) => (
    {
        add() {
            dispatch(addNum(this.props.num))
        },
        decrease() {
            dispatch(decreaseNum(this.props.num))
        }
    }

)

export default connect(mapStateToProps, mapDispatchToProps)(Home)