import appConstants from '../constants/app'

//reducer是唯一能够去更新state值的方式,action并不能改变state,它只是用来告知需要做何种更改,相当于一个指示器.

//initState(初始化state)
const initialState = {
    num: 1
}

export default function app(state = initialState, action) {
    switch (action.type) {
        case appConstants.NUM_ADD:
        case appConstants.NUM_DECREASE:
            return {
                ...state,
                num: action.data.num
            }

        default:
            return state
    }
}