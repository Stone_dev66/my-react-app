import appConstants from '../constants/app'

//定义创建action函数,返回值为一个新的action
export function addNum(num) {
    return {
        type: appConstants.NUM_ADD,
        data: {
            num: num + 1
        }
    }

}

export const decreaseNum = (num) => ({
    type: appConstants.NUM_DECREASE,
    data: {
        num: num - 1
    }
})